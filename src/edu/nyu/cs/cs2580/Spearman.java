package edu.nyu.cs.cs2580;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author Anuj Bora
 */
public class Spearman {
	static private String numviewsPath;
	static private String pagerankPath;
	static private Map<String, Integer> _numViews = new HashMap<String, Integer>();
	static private Map<String, Float> _pageRanks = new HashMap<String, Float>();
	static private Map<String, Integer> _indexedNumViews;
	static private Map<String, Integer> _indexedPageRanks;
	
	public Spearman() {}
	
	private static void loadNumViews() throws FileNotFoundException, IOException, ClassNotFoundException {
		System.out.println("Load Numviews from: " + numviewsPath);
		ObjectInputStream reader = new ObjectInputStream(new FileInputStream(numviewsPath));
		LogMinerNumviews loaded = (LogMinerNumviews) reader.readObject();
		_numViews = loaded.getNumViews();
		loaded = null;
		reader.close();
	}


	private static void loadPageranks() throws FileNotFoundException, IOException, ClassNotFoundException {
		System.out.println("Load Pageranks from: " + pagerankPath);
		ObjectInputStream reader = new ObjectInputStream(new FileInputStream(pagerankPath));
		CorpusAnalyzerPagerank loaded = (CorpusAnalyzerPagerank) reader.readObject();
		_pageRanks = loaded.getPageRanks();
		loaded = null;
		reader.close();
	}
	
	private static Map<String, Integer> sortNumViews(Map<String, Integer> unsortMap) {
    // 1. Convert Map to List of Map
    List<Map.Entry<String, Integer>> list =
            new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

    // 2. Sort list with Collections.sort(), provide a custom Comparator
    //    Try switch the o1 o2 position for a different order
    Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
        public int compare(Map.Entry<String, Integer> o1,
                           Map.Entry<String, Integer> o2) {
            return (o2.getValue()).compareTo(o1.getValue()) * -1;
        }
    });

    // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
    int count = 1;
    Map<String, Integer> indexedMap = new LinkedHashMap<String, Integer>();
    for (Map.Entry<String, Integer> entry : list) {
    	indexedMap.put(entry.getKey(), count);
    	count++;
    }

    return indexedMap;
	}
	
	private static Map<String, Integer> sortPageRanks(Map<String, Float> unsortMap) {
    // 1. Convert Map to List of Map
    List<Map.Entry<String, Float>> list =
            new LinkedList<Map.Entry<String, Float>>(unsortMap.entrySet());

    // 2. Sort list with Collections.sort(), provide a custom Comparator
    //    Try switch the o1 o2 position for a different order
    Collections.sort(list, new Comparator<Map.Entry<String, Float>>() {
        public int compare(Map.Entry<String, Float> o1,
                           Map.Entry<String, Float> o2) {
            return (o2.getValue()).compareTo(o1.getValue()) * -1;
        }
    });

    // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
    Map<String, Integer> indexedMap = new LinkedHashMap<String, Integer>();
    int count = 1;
    for (Map.Entry<String, Float> entry : list) {
    	indexedMap.put(entry.getKey(), count);
    	count++;
    }

    return indexedMap;
	}
	
	private static Float computeSpearman() {
		float z = 0.0f;
		float xK = 0.0f;
		int n = _indexedPageRanks.size();
		
		/*for (Entry<String, Integer> entry : _indexedPageRanks.entrySet()) {
			xK = xK + entry.getValue();
			n++;
		} */
		
		//z = (float) (xK / n);
		z =  ((float) (n+1) / 2);
		xK = 0.0f;
		float numerator = 0.0f;
		float denominator1 = 0.0f;
		float denominator2 = 0.0f;
		
		int count = 0;
		for (Entry<String, Integer> entry : _indexedPageRanks.entrySet()) {
			int pagerankRank = entry.getValue();
			int numviewsRank = _indexedNumViews.get(entry.getKey());
			//int numviewsRank = pagerankRank;
			if (count < 10) {
				System.out.println("PageRank Rank : " + pagerankRank + "NumViews Rank : " + numviewsRank);
			}
			
			numerator = numerator + ((float) ((pagerankRank - z) * (numviewsRank - z)));
			denominator1 = denominator1 + ((float) ((pagerankRank - z) * (pagerankRank - z)));
			denominator2 = denominator2 + ((float) ((numviewsRank - z) * (numviewsRank - z)));
		}
		
		float denominator_mult = ((float) (denominator1 * denominator2));
		float denominator = (float) Math.sqrt(denominator_mult);
		
		System.out.println("z : " + z);
		System.out.println("Numerator : " + numerator);
		System.out.println("Denominator1 : " + denominator1);
		System.out.println("Denominator2 : " + denominator2);
		
		//float result = (float) (numerator / (denominator1 * denominator2)); 
		float result = (float) (numerator / (denominator)); 
		
		return result;
	}
	
	public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException {
		if (args.length != 2) {
			throw new IllegalArgumentException("Invalid arguments");
		}
		
		/**
		 * Obtain paths of the pagerank and numviews file
		 */
		pagerankPath = args[0];
		numviewsPath = args[1];
		
		/**
		 * load the numviews and pageranks data structures
		 */
		loadNumViews();
		loadPageranks();
		
		/**
		 * Sort numviews and pageranks data structures by values of pagerank and numviews
		 * and then return a hashmap of where filename is key and its rank is index
		 */
		_indexedNumViews = sortNumViews(_numViews);
		_indexedPageRanks = sortPageRanks(_pageRanks);
		
		/**
		 * Obtain Spearman Co-efficient
		 */
		Float spearmanCoefficient = computeSpearman();
		
		System.out.println("Spearman Co-efficient is : " + spearmanCoefficient);
	}

}
