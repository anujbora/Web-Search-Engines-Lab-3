package edu.nyu.cs.cs2580;

import java.util.Collections;
import java.util.HashMap;
import java.util.Vector;

import edu.nyu.cs.cs2580.QueryHandler.CgiArguments;
import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Implement this class for HW2 based on a refactoring of your favorite
 * Ranker (except RankerPhrase) from HW1. The new Ranker should no longer rely
 * on the instructors' {@link IndexerFullScan}, instead it should use one of
 * your more efficient implementations.
 */
public class RankerFavorite extends Ranker {

  private double lambda = 0.5;
  
  public RankerFavorite(Options options,
      CgiArguments arguments, Indexer indexer) {
    super(options, arguments, indexer);
    System.out.println("Using Ranker: " + this.getClass().getSimpleName());
    
    //_indexer = (IndexerInvertedCompressed) _indexer;
  }

  @Override
  public Vector<ScoredDocument> runQuery(Query query, int numResults) {
	  Vector<ScoredDocument> all = new Vector<ScoredDocument>();
		
	    HashMap<String,Double> query_probab = new HashMap<String,Double>();
	    
	    long N = _indexer._totalTermFrequency;
	    
	    for(String token: query._tokens) {
	    	
	    	if(!query_probab.containsKey(token)) {
	    		
	    		double value = _indexer.corpusTermFrequency(token);
	    		value = lambda * (value / N);
	    		
	    		query_probab.put(token, value);
	    		
	    	}
	    }
	    
	    Document doc = null;
	    int docid = -1;
	    
	    while ((doc = _indexer.nextDoc(query, docid)) != null) {
	      all.add(scoreDocument(query, query_probab, doc));
	      docid = doc._docid;
	    }
	    
	    if(all.isEmpty()) {
	      return all;
	    }
	    
	    Collections.sort(all, Collections.reverseOrder());
		
		// Normalizing Scores
		double maxScore = all.get(0).get_score();
		double minScore = all.get(all.size()-1).get_score();
		double scoreRange = maxScore - minScore;
		if(maxScore == 0 && minScore == 0){
			all.setSize(numResults);
			return all;	
		}
		Vector<ScoredDocument> results = new Vector<ScoredDocument>();
		for (int i = 0; i < all.size() && i < numResults; ++i) {
		  double score = (all.get(i).get_score() - minScore ) / scoreRange;
		  all.get(i).set_score(score);
		  results.add(all.get(i));
		}
		    
		return results;
  }
  
  private ScoredDocument scoreDocument(Query query, HashMap<String,Double> query_probab, Document document) {
	  
	  double score = 0;
	    
	  int D = ((DocumentIndexed)document).size;
	  for(String token: query._tokens) {
	    	
	    	double value = 0;
	    	
	    	value = _indexer.documentTermFrequency(token, document._docid);
        
	    	value = value / D;
	    	value = (1 - lambda) * value;
	    	value += query_probab.get(token);
	    	if(value != 0)
	    		value = Math.log10(value);
	    	score += value;
	    	
	    }
	    return new ScoredDocument(document, score);
  }
}
