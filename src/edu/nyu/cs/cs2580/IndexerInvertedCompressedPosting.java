package edu.nyu.cs.cs2580;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class IndexerInvertedCompressedPosting implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 7209254948562454281L;
  public List<Integer> _posting = new ArrayList<Integer>();
  public List<Byte> _postingEncoded = new ArrayList<Byte>(); 
  //Points to the last accessed document
  private int cachedIndexOffsetDoc = 0;
  public int lastDocumentNum = -1; 
  public boolean isEncoded = false;
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "IndexerInvertedCompressedPosting [_posting=" + _posting + "]";
  }

  public void encode() {
    if(isEncoded) {
      return;
    }
    encodeDelta();
    _postingEncoded = new ArrayList<Byte>();
    for(Integer i : _posting) {
      List<Byte> output = new ArrayList<Byte>();
      boolean first = true;
      while(i > 127) {
        if (first) {
          output.add((byte)(i | 0x80));
          first = false;
        } else {
          output.add((byte)(i & 0x7F));//output.put(i & 0x7F);
        }
        i>>>=7;
      }
      if (first) {
        output.add((byte)(i | 0x80));
      } else {
         output.add((byte)(i & 0x7F));
      }
      Collections.reverse(output);
      _postingEncoded.addAll(output);
    }
    _posting = null;
    isEncoded = true;
  }
    
  public void decode() {
    //System.out.println("I am decoding");
    if (!isEncoded){
      return;
    }
    
    _posting = new ArrayList<Integer>();
    for (int i=0;i<_postingEncoded.size(); i++) {
      int position = 0;
      int result = 0;
      while(((_postingEncoded.get(i)) & 0x80) == 0){
        int unsignedByte = ((int)_postingEncoded.get(i)&0x7F);
        result = (result << (7*position)) | (unsignedByte);
        i+=1;
        if (position == 0)
          position +=1;
      }
      int unsignedByte = ((int)_postingEncoded.get(i)&0x7F);
      result = (result << (7*position)) | (unsignedByte);
      _posting.add(result);
    }
    decodeDelta();
    _postingEncoded = null;
    isEncoded = false;
  }
  
  public void encodeDelta() {
    int previousValue = _posting.get(0);
    for(int i = 0; i < _posting.size();) {
      if (i != 0) {
        _posting.set(i, _posting.get(i) - previousValue);
        previousValue = _posting.get(i) + previousValue;
      }
      i++;
      int j = 2;
      int previousPos = _posting.get(i + 1);
      while(j <= _posting.get(i)){
        _posting.set(i+j, _posting.get(i+j) - previousPos);
        previousPos = _posting.get(i+j) + previousPos;
        j++;
      }
      i = i+j;
    }
  }
  
  public void decodeDelta() {
    int previousValue = _posting.get(0);
    for(int i = 0; i < _posting.size();) {
      if (i != 0) {
        _posting.set(i, _posting.get(i) + previousValue);
        previousValue = _posting.get(i);
      }
      i++;
      int j = 2;
      int previousPos = _posting.get(i + 1);
      while(j <= _posting.get(i)){
        _posting.set(i+j, _posting.get(i+j) + previousPos);
        previousPos = _posting.get(i+j);
        j++;
      }
      i = i+j;
    }
  }

  public IndexerInvertedCompressedPosting() {
    // TODO Auto-generated constructor stub
  }
  
  public void add(int docid, List<Integer> positions) {
    if (docid <= lastDocumentNum) {
      throw new IllegalArgumentException("'____'");
    }
    if (positions != null) {
      _posting.add(docid);
      lastDocumentNum = docid; 
      _posting.add(positions.size());
      _posting.addAll(positions);
    }
  }
  
  //This returns the next document
  public Integer next(int current) {
    if(isEncoded) {
      decode();
    }
    if(_posting.size() == 0 || lastDocumentNum <= current) {
      return -1;
    }
    //First document number
    if(_posting.get(0) > current || _posting.get(cachedIndexOffsetDoc) > current) {
      cachedIndexOffsetDoc = 0;
    }
    while(_posting.get(cachedIndexOffsetDoc) <= current) {
      // Points to the next document
      cachedIndexOffsetDoc += _posting.get(cachedIndexOffsetDoc + 1) + 2; 
      if (cachedIndexOffsetDoc >= _posting.size()) {
        return -1;
      }
    }
    return _posting.get(cachedIndexOffsetDoc);
  }
  
  public Integer next_pos(int docid, int pos) {
    if(isEncoded) {
      decode();
    }
    if(_posting.size() == 0 || lastDocumentNum < docid) {
      return -1;
    }
    if (cachedIndexOffsetDoc == -1) {
      cachedIndexOffsetDoc = 0;
    }
    if (_posting.get(cachedIndexOffsetDoc) == docid) {
      return next_pos_doc_found(docid, pos);
    }
    if (_posting.get(cachedIndexOffsetDoc) > docid) {
      cachedIndexOffsetDoc = 0;
    }
    while(_posting.get(cachedIndexOffsetDoc) < docid) {
      // Points to the next document
      cachedIndexOffsetDoc += _posting.get(cachedIndexOffsetDoc + 1) + 2; 
      if (cachedIndexOffsetDoc >= _posting.size()) {
        return -1;
      }
    }
    if (_posting.get(cachedIndexOffsetDoc) == docid) {
      return next_pos_doc_found(docid, pos);
    }
    return -1;
  }
  
  // This is called when you found the doc in the array.
  private Integer next_pos_doc_found(int docid, int pos) {
    int i = 0;
    int temp = cachedIndexOffsetDoc + 2;
    while(i < _posting.get(cachedIndexOffsetDoc + 1)) {
      if (_posting.get(temp + i) > pos){
        return _posting.get(temp + i);
      }
      i++;
    }
    return -1;
  }
  
  public int totalTermsInDocument(int docId) {
    if(isEncoded) {
      decode();
    }
    next(docId-1);
    if (_posting.get(cachedIndexOffsetDoc) == docId) {
      return _posting.get(cachedIndexOffsetDoc + 1);
    } else {
      return 0;
    }
  }
}
