package edu.nyu.cs.cs2580;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import edu.nyu.cs.cs2580.SearchEngine.Options;


public class Testing {
	public static void main(String[] args) throws IOException {
		
		CorpusAnalyzerPagerank pagerankTest = new CorpusAnalyzerPagerank();
		
		pagerankTest.prepare();
		pagerankTest.compute();
		
		
	}
}
