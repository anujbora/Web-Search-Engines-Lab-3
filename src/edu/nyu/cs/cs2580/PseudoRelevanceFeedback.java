package edu.nyu.cs.cs2580;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Vector;

public class PseudoRelevanceFeedback {
	
	int maxDocs;
	int maxTerms;
	Ranker ranker;
	Indexer indexer;
	Query query;
	HashSet<String> stopWords;
	
	PseudoRelevanceFeedback(int k, int m, Ranker rank, Indexer ind, Query query) throws IOException {
		
		this.maxDocs = k;
		this.maxTerms = m;
		this.ranker = rank;
		this.indexer = ind;
		this.query = query;
		
		
		stopWords = new HashSet<String>();
		
		File stopWordsFile = new File("StopWords.txt");
		BufferedReader br= new BufferedReader(new FileReader(stopWordsFile));
		String line = "";
		RawDocumentParser parser = new RawDocumentParser();
		while ((line = br.readLine()) != null) {
			
			stopWords.add(parser.cleanAndStem(line.trim()));
		}
		
		br.close();
		
	}
	
	
	public ArrayList<ScoredTerm> run() throws IOException {
		
		ArrayList<ScoredTerm> result = new ArrayList<ScoredTerm>();
		
		HashMap<String, Integer> termRankedDocFreq = new HashMap<String, Integer>();
		Vector<ScoredDocument> rankedDocuments = ranker.runQuery(query, maxDocs);
		
		HashSet<String> files = new HashSet<String>();
		
		String corpusPrefix = indexer._options._corpusPrefix;
		
		for(ScoredDocument sd : rankedDocuments) {
			Document d = sd.get_doc();
			String docUrl = d.getUrl();
			
			String fileName = docUrl.replaceAll("https://en.wikipedia.org/wiki/", corpusPrefix+"/");
			files.add(fileName);
		}	
		
		RawDocumentParser parser = new RawDocumentParser();
		
		double totalterms = 0D;
		
		for(String f: files) {
			
			File rankedFile = new File(f);
			String line =  parser.getConvertedDocument(rankedFile);
			
			String body = line.split("\t")[2];
			
			Scanner s = new Scanner(body);
			
			while (s.hasNext()){
				
				String term = s.next();
				
				if(!stopWords.contains(term)) {
					
					totalterms++;
					
					if(termRankedDocFreq.containsKey(term)) {
						
						termRankedDocFreq.put(term, termRankedDocFreq.get(term)+1);
						
					} else {
						
						termRankedDocFreq.put(term,1);
						
					}
					
				}
				
			}
			s.close();
		}
			
		ArrayList<ScoredTerm> resultTemp = new ArrayList<ScoredTerm>();
			
		for(String k: termRankedDocFreq.keySet()) {
			
			ScoredTerm sd = new ScoredTerm(k, ((double) termRankedDocFreq.get(k)) / totalterms);
				
			resultTemp.add(sd);
		}
			
		Collections.sort(resultTemp, Collections.reverseOrder());
		
		Double normalizingSum = 0D;
			
		for(int i = 0; (i < resultTemp.size() && i < maxTerms); i++) {
				
			result.add(resultTemp.get(i));
			normalizingSum += resultTemp.get(i).get_score();
		}
			
		for(int i = 0; (i < result.size() && i < maxTerms); i++) {
				
			result.get(i).set_score(result.get(i).get_score() / normalizingSum);
		}
		
		return result;
	}
	

}
