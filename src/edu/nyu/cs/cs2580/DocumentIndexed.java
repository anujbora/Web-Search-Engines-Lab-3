package edu.nyu.cs.cs2580;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @CS2580: implement this class for HW2 to incorporate any additional
 * information needed for your favorite ranker.
 */
public class DocumentIndexed extends Document {
  private static final long serialVersionUID = 9184892508124423115L;
  public int size = 0;
  private String fileName;
  private Map<String, Float> outgoingLinks;
  private int outgoingLinksCount = 0;
  private Map<String, Float> incomingLinks = new HashMap<String, Float>();
  
  //private Map<String, Float> DincomingLinks = new HashMap<String, Float>();
  //private Map<String, Float> DoutgoingLinks = new HashMap<String, Float>();
  
  
  
  public DocumentIndexed(int docid) {
    super(docid);
  }
  
  public float calculatePageRankSingleIteration( ArrayList<String> docNames,float lambda) {
    //HashMap<Integer, Float> result = new HashMap<Integer, Float>();
    int N = docNames.size();
  	float pageRank = 0;
    for(int i = 0; i < N; i++) {
      if (incomingLinks.containsKey(docNames.get(i))) {
        //DincomingLinks.put(docNames.get(i), (float) (lambda * incomingLinks.get(docNames.get(i)) + (1 - lambda) * (1.0/N)) );
      	pageRank += (float) (lambda * incomingLinks.get(docNames.get(i)) + (1 - lambda) * (1.0/N));
      }
      else {
      	//DincomingLinks.put(docNames.get(i), (float) ((1 - lambda) * (1.0/N)));
      	pageRank += (float) ((1 - lambda) * (1.0/N));
      }
    }
    return pageRank;
  }
  
  /*public void calculateDoutgoingLinks(ArrayList<String> docNames,float lambda) {
  //HashMap<Integer, Float> result = new HashMap<Integer, Float>();
    int N = docNames.size();
  	for(int i = 0; i < N; i++) {
      if (outgoingLinks.containsKey(docNames.get(i))) {
        DoutgoingLinks.put(docNames.get(i), (float) (lambda * outgoingLinks.get(docNames.get(i)) + (1 - lambda) * (1.0/N)) );
      }
      else {
      	DoutgoingLinks.put(docNames.get(i), (float) ((1 - lambda) * (1.0/N)));
      }
    }
  }*/
  
  
  /*public float calculatePageRank(Map<String, DocumentIndexed> _corpus){
    float pageRank = 0;
    //System.out.println(this.fileName + ":");
    //System.out.println(DincomingLinks);
    for(Entry<String, DocumentIndexed> doc : _corpus.entrySet()) {
      pageRank += multiply(DincomingLinks, doc.getValue().DoutgoingLinks);
    }
    return pageRank;
  }*/
  
  
  
  public float calculatePageRank(Map<String, DocumentIndexed> _corpus, float lambda, int iteration) {
  	if (iteration == 1) {
  		return calculatePageRankSingleIteration(new ArrayList<String>(_corpus.keySet()), lambda);
  	}
  	else {
  		return calculatePageRankDoubleIteration(_corpus, lambda);
  	}
  }
  
  private float calculatePageRankDoubleIteration(Map<String, DocumentIndexed> _corpus, float lambda) {
    int N = _corpus.size();
  	float pageRank = 0;
    for(Entry<String, DocumentIndexed> doc : _corpus.entrySet()) {
      pageRank += multiply(incomingLinks, doc.getValue().outgoingLinks);
    }
    pageRank = pageRank * lambda * lambda;
    for(String key: incomingLinks.keySet()) {
      pageRank += incomingLinks.get(key) * lambda * (1-lambda);
    }
    
    pageRank +=  (1-lambda) * lambda;
    pageRank += ((1 - lambda) * (1-lambda));
    return pageRank;
  }
  
  
  
  
  
  
  
  
  
  
  
  private float multiply(Map<String, Float> links1, Map<String, Float> links2) {
    float result = 0;
    for(String key: links1.keySet()) {
      if(links2.containsKey(key)) {
        result += (links1.get(key) * links2.get(key));
      }
    }
    return result;  
  }
  
  public void setFileName(String fileName) {
  	this.fileName = fileName;
  }
  
  public String getFileName() {
  	return this.fileName;
  }
  
  public void setOutgoingLinks(Map<String, Float> outgoingLinks) {
		this.outgoingLinks = new HashMap<String, Float>(outgoingLinks);
	}
  
  public Map<String, Float> getOutgoingLinks() {
  	return this.outgoingLinks;
  }

	public int getOutgoingLinksCount() {
		return outgoingLinksCount;
	}

	public void setOutgoingLinksCount(int outgoingLinksCount) {
		this.outgoingLinksCount = outgoingLinksCount;
	}

	public Map<String, Float> getIncomingLinks() {
		return incomingLinks;
	}

	public void addIncomingLink(String doc, Float probability) {
		this.incomingLinks.put(doc, probability);
	}

}
