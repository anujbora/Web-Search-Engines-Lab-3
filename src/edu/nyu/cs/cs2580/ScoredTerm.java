package edu.nyu.cs.cs2580;

class ScoredTerm implements Comparable<ScoredTerm> {
  
  private String _term;
  private double _score;

  public ScoredTerm(String term, double score) {
    _term = term;
    _score = score;
  }
  
  public double get_score() {
		return _score;
  }

  public void set_score(double _score) {
		this._score = _score;
  }
  
  public String get_term() {
		return _term;
  }

  public void set_term(String t) {
		this._term = t;
  }

  public String asTextResult() {
    StringBuffer buf = new StringBuffer();
    buf.append(_term).append("\t");
    buf.append(_score);
    return buf.toString();
  }

  /**
   * @CS2580: Student should implement {@code asHtmlResult} for final project.
   */
  public String asHtmlResult() {
    return "";
  }

  @Override
  public int compareTo(ScoredTerm o) {
    if (this._score == o._score) {
      return 0;
    }
    return (this._score > o._score) ? 1 : -1;
  }
}
