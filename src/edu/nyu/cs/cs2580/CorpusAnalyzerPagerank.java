package edu.nyu.cs.cs2580;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Implement this class for HW3.
 */
public class CorpusAnalyzerPagerank extends CorpusAnalyzer implements Serializable {
	private static final long serialVersionUID = 3786445882920299430L;
	private int _corpusSize = 0;
	private float LAMBDA = (float) 0.1;
	// Iteration can be either 1 or 2. Not supported for more than 2.
	private int ITERATION = 2;
  private Map<String, DocumentIndexed> _corpusDocuments = 
  		new HashMap<String, DocumentIndexed>();
  
  private Map<String, Float> _pageRanks = 
  		new HashMap<String, Float>();
  
  private Set<String> htmlPages = new HashSet<String>();
  private Map<String, String> redirects = new HashMap<String, String>();
  
  /**
   * Testing
   */
  public CorpusAnalyzerPagerank() {
  	
  }
  
	public CorpusAnalyzerPagerank(Options options) {
    super(options);
  }

  /**
   * This function processes the corpus as specified inside {@link _options}
   * and extracts the "internal" graph structure from the pages inside the
   * corpus. Internal means we only store links between two pages that are both
   * inside the corpus.
   * 
   * Note that you will not be implementing a real crawler. Instead, the corpus
   * you are processing can be simply read from the disk. All you need to do is
   * reading the files one by one, parsing them, extracting the links for them,
   * and computing the graph composed of all and only links that connect two
   * pages that are both in the corpus.
   * 
   * Note that you will need to design the data structure for storing the
   * resulting graph, which will be used by the {@link compute} function. Since
   * the graph may be large, it may be necessary to store partial graphs to
   * disk before producing the final graph.
   *
   * @throws IOException
   */
  @Override
  public void prepare() throws IOException {
    System.out.println("Preparing " + this.getClass().getName());
    
    //String path = "data/wiki";
    String path = _options._corpusPrefix;

    File folder = new File(path);
    File[] listOfFiles = folder.listFiles();

    for (File f : listOfFiles) {
    		if (f.isHidden() || f.isDirectory()) {
	      		continue;	
	      	}
	      	if (!f.isFile()) {
	      		continue;
	      	}

		  	if (isValidDocument(f)) {
			  	DocumentIndexed d = new DocumentIndexed(_corpusSize);
			  	d.setFileName(f.getName());
			  	_corpusDocuments.put(f.getName(), d);
			  	_corpusSize++;
		  	}
    }
    int count = 0;
    for (File f : listOfFiles) {
    	if (!isValidDocument(f)) {
    		continue;
    	}

    	if (f.isHidden() || f.isDirectory()) {
	      	continue;	
	    }
	    if (!f.isFile()) {
	      	continue;
	    }
	    
	    count++;
    	HeuristicLinkExtractor extractor = new HeuristicLinkExtractor(f);
    	
    	if(extractor.getLinkSource().endsWith(".html"))
    	{
    		htmlPages.add(f.getName());
    	}
    	
    	int outgoingLinksCount = 0;
    	Map<String, Float> outgoingLinks = new HashMap<String, Float>();
    	
    	String link;
    	while ((link = extractor.getNextInCorpusLinkTarget()) != null) {
				
    		if (_corpusDocuments.containsKey(link)) {
    			//System.out.println("From : " + f.getName() + " To : " + link);
					
    			// TODO : Check Piazza answer
    			if (!outgoingLinks.containsKey(link)) {
						outgoingLinksCount++;
						outgoingLinks.put(link, 0.0f);
					}
					
					/**
					 * Add incoming link in document "link"
					 */
					//DocumentIndexed doc = _corpusDocuments.get(link);
					//doc.addIncomingLink(f.getName(), 0.0f);
				}
			}
    	
    	for (Map.Entry<String, Float> entry : outgoingLinks.entrySet()) {
    		outgoingLinks.put(entry.getKey(), (float) (1.0 / outgoingLinksCount));
    	}
    	
    	DocumentIndexed doc = _corpusDocuments.get(f.getName());
    	doc.setOutgoingLinks(outgoingLinks);
    	doc.setOutgoingLinksCount(outgoingLinksCount);
    	outgoingLinks.clear();
    	
   if (count % 1000 == 0) {
  	 System.out.println("Prepared " + count + " documents");
   }
    		
    	
    }
    
    
    /**
     * Update redirects
     */
    /*
    for (String page : htmlPages) {
      String redirect_from = page.substring(0, page.length() - 5);
      if( _corpusDocuments.containsKey(redirect_from)) {
      	redirects.put(redirect_from, page);
      }	
  	}
    
    for (Map.Entry<String, String> r : redirects.entrySet()) {
    	if (_corpusDocuments.containsKey(r.getKey())) {
    		DocumentIndexed d = _corpusDocuments.get(r.getKey());
    		
    	}
    }
    
    for (Map.Entry<String, DocumentIndexed> entry : _corpusDocuments.entrySet()) {
    	DocumentIndexed d = entry.getValue();
    	 if (d.getOutgoingLinksCount() == 0) {
    		 continue;
    	 }
    	 
    	 Map<String, Float> outgoing = d.getOutgoingLinks();
    	 
    	 for (Entry<String, Float> e : outgoing.entrySet()) {
    		 if (redirects.containsKey(e.getKey())) {
    			 Float val = e.getValue();
    			 String new_destination = redirects.get(e.getKey());
    			 outgoing.remove(e.getKey());
    			 outgoing.put(new_destination, val);
    		 }
    	 }
    	
    }
    
    */
    
    /*
    for (Map.Entry<String, DocumentIndexed> entry : _corpusDocuments.entrySet()) {
  		int incomingLinksCount = entry.getValue().getIncomingLinks().size();
  		
  		if (incomingLinksCount == 0) {
  			continue;
  		}
  		
  		for (Map.Entry<String, Float> linkentry : entry.getValue().getIncomingLinks().entrySet()) {
    		linkentry.setValue((float) (1.0 / incomingLinksCount));
    	}
  	}
  	*/
    for (Map.Entry<String, DocumentIndexed> entry : _corpusDocuments.entrySet()) {
    	if (entry.getValue().getOutgoingLinksCount() == 0) {
    		continue;
    	}
    	
    	String from = entry.getKey();
    	Map<String, Float> outgoing = entry.getValue().getOutgoingLinks();
    	for (Map.Entry<String, Float> e : outgoing.entrySet()) {
    		if (_corpusDocuments.containsKey(e.getKey())) {
    			DocumentIndexed doc = _corpusDocuments.get(e.getKey());
      		doc.addIncomingLink(from, e.getValue());
    		}
    		
    	}
    }
    
    System.out.println(
        "Mined Corpus with " + _corpusSize + " documents"
        );
    
    /**
     * TEST Outgoing links
     */
    /*System.out.println("Output of Outgoing Links : ");
    for (Map.Entry<String, DocumentIndexed> entry : _corpusDocuments.entrySet()) {
    	if (entry.getValue().getOutgoingLinksCount() != 0) {
    		System.out.println(entry.getKey() + " => " + entry.getValue().getOutgoingLinks());
    	}
    }*/
    
    
    /**
     * TEST Incoming Links
     *//*
    System.out.println("Output of Incoming Links : ");
    for (Map.Entry<String, DocumentIndexed> entry : _corpusDocuments.entrySet()) {
    	if (entry.getValue().getIncomingLinks().size() != 0) {
    		System.out.println(entry.getKey() + " has inlinks from  " + entry.getValue().getIncomingLinks());
    	}
    }*/
    
    // Store the values to disk
    //String indexFile = _options._indexPrefix + "/pagerank.idx";
    
    /*
    String indexFile = "data/index/pagerank.idx";
    System.out.println("Store PageRank index to: " + indexFile);
    ObjectOutputStream writer =
        new ObjectOutputStream(new FileOutputStream(indexFile));
    writer.writeObject(this);
    
    _corpusDocuments.clear();
    _corpusDocuments = null;
    */
    return;
  }

  /**
   * This function computes the PageRank based on the internal graph generated
   * by the {@link prepare} function, and stores the PageRank to be used for
   * ranking.
   * 
   * Note that you will have to store the computed PageRank with each document
   * the same way you do the indexing for HW2. I.e., the PageRank information
   * becomes part of the index and can be used for ranking in serve mode. Thus,
   * you should store the whatever is needed inside the same directory as
   * specified by _indexPrefix inside {@link _options}.
   *
   * @throws IOException
   */
  @Override
  public void compute() throws IOException {
  	System.out.println("Computing using " + this.getClass().getName());
  	int count = 0;
  	//for(Entry<String, DocumentIndexed> entry: _corpusDocuments.entrySet()) {
  	//	count++;
    //	entry.getValue().calculateDincomingLinks(new ArrayList<String>(_corpusDocuments.keySet()), LAMBDA);
    //	entry.getValue().calculateDoutgoingLinks(new ArrayList<String>(_corpusDocuments.keySet()), LAMBDA);

    //		System.out.println("Computed PageRank for " + count + "documents");
    	
    //}
    count = 0;
    for(Entry<String, DocumentIndexed> entry: _corpusDocuments.entrySet()) {
    	count++;
    	//_pageRanks.put(entry.getKey(), entry.getValue().calculatePageRank(_corpusDocuments));
    	_pageRanks.put(entry.getKey(), entry.getValue().calculatePageRank(_corpusDocuments, LAMBDA, ITERATION));
    	if(count % 1000 == 0) {
    		System.out.println("Calculated PageRank for " + count + " documents");
    	}
    }
  
    System.out.println(
        "Mined Corpus with " + _corpusSize + " documents and generated page ranks"
        );
    
    // Store the values to disk
    /**
     * TODO TESTING
     */
    //String indexFile = "data/index/pagerank.idx";
    String indexFile = _options._indexPrefix + "/pagerank.idx";
    System.out.println("Store PageRank index to: " + indexFile);
    ObjectOutputStream writer =
        new ObjectOutputStream(new FileOutputStream(indexFile));
    writer.writeObject(this);
    
    //System.out.println(_pageRanks);
    
    //System.out.println(_pageRanks);
    _corpusDocuments.clear();
    _pageRanks.clear();
    //get_numViews().clear();
    //set_numViews(null);
    return;
  }

  /**
   * During indexing mode, this function loads the PageRank values computed
   * during mining mode to be used by the indexer.
   *
   * @throws IOException
   */
  @Override
  public Object load() throws IOException {
    System.out.println("Loading using " + this.getClass().getName());
    
    String indexFile = _options._indexPrefix + "/pagerank.idx";
    System.out.println("Load index from: " + indexFile);
    ObjectInputStream reader =
        new ObjectInputStream(new FileInputStream(indexFile));
    CorpusAnalyzerPagerank loaded = null;
		try {
			loaded = (CorpusAnalyzerPagerank) reader.readObject();
			this._pageRanks = loaded._pageRanks;
	    System.out.println("Loaded All the documents' page ranks");
	    loaded = null;
		} catch (ClassNotFoundException ignore) {
				// ignored as this exception will not happen
		}
    reader.close();
    reader = null;
    
    return _pageRanks;
    
  }

	public Map<String, Float> getPageRanks() {
		return _pageRanks;
	}
}
