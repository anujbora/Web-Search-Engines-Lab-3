package edu.nyu.cs.cs2580;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Bhattacharyya {

	public static void main(String[] args) throws Exception {
		String inputFilePath = null;
		String outputFilePath = null;
		ArrayList<String> queries = new ArrayList<String>();
		HashMap<String, String> queryFileMap = new HashMap<String, String>();

		if(args.length != 2)
		{
			throw new Exception("Input path and Output path Required.");
		}
		else
		{
			inputFilePath = args[0];
			outputFilePath = args[1];
			
		}
		
		
		File inputFile = new File(inputFilePath);
		BufferedReader br= new BufferedReader(new FileReader(inputFile));
		String line = "";
		
		while ((line = br.readLine()) != null) {
			
			String[] array = line.split(":");

			queries.add(array[0]);
			queryFileMap.put(array[0],array[1]);
			
		}
		
		br.close();
		
		PrintWriter writer = new PrintWriter(outputFilePath,"UTF-8");
		
		for(int i = 0; i<queries.size(); i++) {
			
			HashMap<String, Double> query1Map = parsePrfFile(queryFileMap.get(queries.get(i)));
			
			for(int j = i+1; j<queries.size(); j++) {
				
				HashMap<String, Double> query2Map = parsePrfFile(queryFileMap.get(queries.get(j)));
				
				double bhattacharyaValue = 0D;
				
				for(String s : query1Map.keySet()) {
					
					if(query2Map.containsKey(s)) {
						
						bhattacharyaValue += Math.sqrt(query1Map.get(s) * query2Map.get(s));		
					}
					
				}
				
				writer.println(queries.get(i) + "\t" + queries.get(j) + "\t" + bhattacharyaValue);
				
			}
			
		}
		
		writer.close();
		
	}
	
	private static HashMap<String, Double> parsePrfFile(String filename) throws FileNotFoundException {
		
		HashMap<String, Double> queryPrf = new HashMap<String, Double>();
		
		Scanner scan = new Scanner(new File(filename));
		
		while (scan.hasNext()) {
			String[] value = scan.nextLine().split("\t");
			queryPrf.put(value[0], Double.parseDouble(value[1]));
		}
		
		scan.close();
		
		return queryPrf;
		
	}
	

}
