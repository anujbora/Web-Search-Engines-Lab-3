package edu.nyu.cs.cs2580;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import edu.nyu.cs.cs2580.SearchEngine.Options;

/**
 * @CS2580: Implement this class for HW3.
 */
public class LogMinerNumviews extends LogMiner implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3876282296761172333L;
	
	//private Map<Integer, Integer> docNumViews = new HashMap<Integer, Integer>();
	private Map<String, Integer> _numViews = new HashMap<String, Integer>(); 
	
  public LogMinerNumviews(Options options) {
    super(options);
  }

  /**
   * This function processes the logs within the log directory as specified by
   * the {@link _options}. The logs are obtained from Wikipedia dumps and have
   * the following format per line: [language]<space>[article]<space>[#views].
   * Those view information are to be extracted for documents in our corpus and
   * stored somewhere to be used during indexing.
   *
   * Note that the log contains view information for all articles in Wikipedia
   * and it is necessary to locate the information about articles within our
   * corpus.
   * @see http://www.baeldung.com/java-read-lines-large-file
   * @throws IOException
   */
  @Override
  public void compute() throws IOException {
    System.out.println("Computing using " + this.getClass().getName());
    
    getAllDocuments();
    
    String path = _options._logPrefix;
    File folder = new File(path);
    
    int i = 0;
    int temp = 0;
    for (File file : folder.listFiles()) {
    	if (file.isHidden() || file.isDirectory()) {
    		continue;	
    	}
    	if (!file.isFile()) {
    		continue;
    	}

	    FileInputStream inputStream = null;
	    Scanner sc = null;
	    String filePath = path + "/" + file.getName();
	    try {
	        inputStream = new FileInputStream(filePath);
	        sc = new Scanner(inputStream, "UTF-8");
	        while (sc.hasNextLine()) {
	            String[] line = sc.nextLine().split(" ");
	            if (line.length == 3) {
		            if (_numViews.containsKey(line[1])) {
		            	int count = _numViews.get(line[1]);
		            	try {
		            		count = count + Integer.parseInt(line[2]);
		    					} catch (NumberFormatException nfe) {
		    						continue;
		    					}
		            	_numViews.put(line[1], count);
		            }
	            }
	            i++;
	        }
	        // note that Scanner suppresses exceptions
	        if (sc.ioException() != null) {
	            throw sc.ioException();
	        }
	    } finally {
	        if (inputStream != null) {
	            inputStream.close();
	        }
	        if (sc != null) {
	            sc.close();
	        }
	    }
    }
    
    System.out.println(
        "Mined Log file with " + Integer.toString(i) + " logs"
        );
    
    // Store the values to disk
    String indexFile = _options._indexPrefix + "/numviews.idx";
    System.out.println("Store NumViews index to: " + indexFile);
    ObjectOutputStream writer =
        new ObjectOutputStream(new FileOutputStream(indexFile));
    writer.writeObject(this);
    
    for (Map.Entry<String, Integer> entry : _numViews.entrySet()) {
    	System.out.println(entry.getKey() + " => " + entry.getValue());
    }
    
    _numViews.clear();
    
    return;
  }
  
  private void getAllDocuments() {
  	String path = _options._corpusPrefix;
  	
  	File folder = new File(path);
  	
    for (File file : folder.listFiles()) {
      if (file.isHidden() || file.isDirectory()) {
        continue; 
      }
      if (!file.isFile()) {
        continue;
      }
      
      _numViews.put(file.getName(), 0);
    }
    
    folder = null;
    
    return;
	}

	/**
   * During indexing mode, this function loads the NumViews values computed
   * during mining mode to be used by the indexer.
   * 
   * @throws IOException
	 * @throws ClassNotFoundException 
   */
  @Override
  public Object load() throws IOException {
    System.out.println("Loading using " + this.getClass().getName());
    String indexFile = _options._indexPrefix + "/numviews.idx";
    System.out.println("Load index from: " + indexFile);
    ObjectInputStream reader =
        new ObjectInputStream(new FileInputStream(indexFile));
    LogMinerNumviews loaded = null;
		try {
			loaded = (LogMinerNumviews) reader.readObject();
			this._numViews = loaded._numViews;
	    System.out.println("Loaded All the documents' views");
	    loaded = null;
		} catch (ClassNotFoundException ignore) {
				// ignored as this exception will not happen
		}
    reader.close();
    reader = null;
    
    return _numViews;
  }
  
  /**
   * Returns number of views the document has
   * @param document name 
   * @return number of views to that document. 0 if document not found
   */
  public int getDocumentViews(String doc) {
  	if (_numViews.containsKey(doc)) {
  		return _numViews.get(doc);
  	}
  	
  	return 0;
  }

	public Map<String, Integer> getNumViews() {
		return _numViews;
	}
  
}
